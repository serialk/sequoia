import collections
import numpy as np
import operator
import scipy.io
import sys
import time

from math import log
from sklearn import metrics
from sklearn.datasets import fetch_mldata
from sklearn.ensemble import RandomForestClassifier

#mnist = fetch_mldata('MNIST original', data_home=".")

mnist = scipy.io.loadmat('mldata/mnist-original.mat')

TEST_IND = 60000
IMG_SIZE = 784
NB_CLASSES = 10

data = mnist["data"].transpose()[:TEST_IND]
data_target = mnist["label"].transpose()[:TEST_IND]
test = mnist["data"].transpose()[TEST_IND:]
test_target = mnist["label"].transpose()[TEST_IND:]

## TO REMOVE FOR FULL SET
'''
small_data = []
small_test = []
small_data_target = []
small_test_target = []
for j in [0, 6500, 13000, 19500, 25000, 31500, 38000, 44500, 51000, 57500]:
    for i in range(100):
        small_data.append(data[i + j])
        small_data_target.append(data_target[i + j])

for j in [0, 1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000]:
    for i in range(200):
        small_test.append(test[i + j])
        small_test_target.append(test_target[i + j])

data = small_data
test = small_test
test_target = small_test_target
data_target = small_data_target
'''
## END TO REMOVE

binarize = lambda x: x > 127
data_target = list(map(int, data_target))
test_target = list(map(int, test_target))

data = list(map(binarize, data))
test = list(map(binarize, test))

class_to_img = collections.defaultdict(list)
for k, v in enumerate(data_target):
    class_to_img[v].append(k)

# decision tree basic : 0.88
# random forest : 0.95

# Arbre :
#  - Les feuilles sont des entiers correspondant à la classe reconnue
#  - Les nœuds internes sont le critère et un arbre binaire (cas 0 et cas 1)

class Node:
    def __init__(self, imgset, pixelset, algo='ig'):
        self.imgset = imgset
        self.pixelset = pixelset
        self.algo = algo

        self.pixel = None
        self.children = None # quand rempli, vaut [cas 0, cas 1] sinon feuille

        self.create_counters()
        self.build_tree()
        self.cleanup_mem()

    def create_counters(self):
        class_to_sumimg = {k: (sum(data[e] for e in v if e in self.imgset))
                        for k, v in class_to_img.items()}
        # pixel -> [nb pixel à 1 pour chaque classe de 0 à 10]
        self.counter = {p: [class_to_sumimg[c][p]
                          if not isinstance(class_to_sumimg[c], int) else 0
                         for c in range(NB_CLASSES)]
                        for p in self.pixelset}
        # [nb images à 1 pour chaque classe de 0 à 10]
        self.numbers = [sum(1 for i in class_to_img[c] if i in self.imgset)
                        for c in range(NB_CLASSES)]

    def cleanup_mem(self):
        self.counter = None
        self.imgset = None
        self.pixelset = None

    @property
    def prediction(self):
        '''Class with the most images in it'''
        return max(enumerate(self.numbers), key=operator.itemgetter(1))[0]

    def same_classes(self):
        first = data_target[next(iter(self.imgset))]
        return all(data_target[img] == first for img in self.imgset)

    def information_gain(self, pixel):
        v = self.counter[pixel]
        v_inv = [self.numbers[i] - v[i] for i in range(NB_CLASSES)]
        size = len(self.imgset)

        def ig():
            return sum(sum((v[i] / size) *
                           log((v[i] * size) / (sv * self.numbers[i]), 2)
                           for i in range(NB_CLASSES) if v[i] != 0)
                       for v, sv in ((v, sum(v)), (v_inv, sum(v_inv))))

        def iv():
            return sum((-(sv / size) * log((sv / size) , 2)
                        if sv != 0 else 0)
                       for sv in (sum(v), sum(v_inv)))

        def gain_ratio():
            iv_val = iv()
            if iv_val < 0.000001:
                return 0
            return ig() / iv_val


        def entropy():
            return sum(sum(-(v[i] / sv) * log(v[i] / sv, 2)
                           for i in range(NB_CLASSES) if v[i] != 0)
                       for v, sv in ((v, sum(v)), (v_inv, sum(v_inv))))

        def gini():
            return sum(1 -
                       sum((v[i] / sv) ** 2
                           for i in range(NB_CLASSES) if v[i] != 0)
                       for v, sv in ((v, sum(v)), (v_inv, sum(v_inv))))

        return {'ig': ig, 'gain_ratio': gain_ratio,
                'entropy': entropy, 'gini': gini}[self.algo]()

    def best_attribute(self):
        return max(self.pixelset, key=self.information_gain)

    def build_tree(self):
        if not self.pixelset or not self.imgset or self.same_classes():
            return # Feuille

        self.pixel = self.best_attribute()

        new_pixelset = set(x for x in self.pixelset if x != self.pixel)
        assert(len(new_pixelset) < len(self.pixelset))

        new_imgset_yes, new_imgset_no = set(), set()
        for img in self.imgset:
            if data[img][self.pixel]:
                new_imgset_yes.add(img)
            else:
                new_imgset_no.add(img)
        self.children = [
            Node(new_imgset_no, new_pixelset),
            Node(new_imgset_yes, new_pixelset)
        ]

    def classify(self, img): # takes an actual image, not an index!
        if not any(self.numbers):
            return None
        if self.children is None:
            return self.prediction
        r = self.children[img[self.pixel]].classify(img)
        if r is not None:
            return r
        else:
            return self.children[not img[self.pixel]].classify(img)


try:
    algo = sys.argv[1]
except IndexError:
    algo = 'ig'

tree = Node(set(range(len(data))), set(range(IMG_SIZE)), algo)


def latex_stats(expected, predicted, algo):
    from sklearn.metrics import confusion_matrix
    confusion = confusion_matrix(expected, predicted)
    precision = [confusion[i][i] / sum(confusion[j][i] for j in range(10)) for i in range(10)]
    recall = [confusion[i][i] / sum(confusion[i][j] for j in range(10)) for i in range(10)]
    print("\\begin{{frame}}{{Decision Tree}}{{Information Gain : \\textbf{{{}}}}}".format(algo))
    print("\\textbf{{Precision}}: {:.5f}\\hfill\\textbf{{Recall:}} {:.5f}".format(np.mean(precision), np.mean(recall)))
    print("\\begin{{center}}\n\\begin{{adjustbox}}{{max width=\\textwidth}}\n\\begin{{tabular}}{{|>{{\columncolor[gray]{{0.85}}}}l|{}}}".format("c|" * 12))
    print("    \\hline\n    \\rowcolor[gray]{{0.85}}  & {} \\\\".format(" & ".join([str(i) for i in range(10)] + ["Precision", "Recall"])))
    def color(n, i, j):
        if n != 0:
            return "\\textbf{{\\textcolor{{{}}}{{{}}}}}".format("green!50!black" if i == j else "red", n)
        return str(n)
    for i, l in enumerate(confusion):
        print("    \\hline\n    {} & {} & {:.3f} & {:.3f}\\\\".format(i, " & ".join(color(n, i, j) for j, n in enumerate(l)), precision[i], recall[i]))
    print("    \\hline\n  \\end{tabular}\n\\end{adjustbox}\n\\end{center}\n\\end{frame}")

expected, predicted = [], []

for i, e in enumerate(test):
    predicted.append(tree.classify(e))
    expected.append(test_target[i])

latex_stats(expected, predicted, {'ig': 'Information Gain',
                                  'gain_ratio': 'Gain Ratio',
                                  'entropy': 'Entropy',
                                  'gini': 'Gini'}[algo])
